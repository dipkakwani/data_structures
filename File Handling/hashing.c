#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>
#define MAX 10
struct student
{
	char USN[11];
	char name[20];
	float marks;
	int att;
} s[MAX];
void read_file(char file_name[]);
void write_file(char file_name[]);
void append_record();
void search_record();
void display();
size_t hash(char usn[]);
void main()
{
	char file_name[20];
	FILE *fp;
	int choice;
	scanf("%s", file_name);
	read_file(file_name);
	while (1)
	{
		printf(" 1.Insert\n 2.Search\n 3.Display\n 4.Exit\n");
		scanf("%d", &choice);
		switch (choice)
		{
			case 1:
				append_record();
				break;
			case 2:
				search_record();
				break;
			case 3:
				display();
				break;
			default:
				write_file(file_name);
				exit(0);
		}
	}
}
void read_file(char file_name[])
{
	FILE *fp;
	int i;
	for (i = 0; i < MAX; i++)
	{
		s[i].USN[0] = '\0';
		s[i].name[0] = '\0';
		s[i].marks = 0.0;
		s[i].att = 0;
	}
	fp = fopen(file_name, "r+");
	if (fp != NULL)
	{
		for (i = 0; i < MAX; i++)
			fread(&s[i], sizeof(s[i]), 1, fp);
		fclose(fp);
	}	
}
void write_file(char file_name[])
{
	FILE *fp;
	int i;
	fp = fopen(file_name, "w+");
	if (fp == NULL)
	{
		printf("File can not be opened\n");
		exit(0);
	}
	for (i = 0; i < MAX; i++)
		fwrite(&s[i], sizeof(s[i]), 1, fp);
	fclose(fp);
}
void append_record()
{
	int index, i, hash_index;
	struct student temp;
	printf("Enter USN\n");
	scanf("%s", temp.USN);
	printf("Enter Name\n");
	scanf("%s", temp.name);
	printf("Enter marks\n");
	scanf("%f", &temp.marks);
	printf("Enter attendance\n");
	scanf("%d", &temp.att);
	hash_index = hash(temp.USN);
	if (s[hash_index].USN[0] != 0)	//Collision - boom!
	{
		index = hash_index;
		//Open addressing - Quadratic Probing
		for (i = 1; s[hash_index].USN[0] != 0; i++)
		{
			hash_index = (int)(index + pow(-1, i - 1) * pow((i + 1) / 2, 2)) % MAX;
			//For linear probing:
			//hash_index = (index + i) % MAX;
			if (hash_index < 0)
				hash_index += MAX;
		}
	}
	s[hash_index] = temp;
}
void search_record()
{
	char usn[11];
	size_t hash_index;
	int h_idx, index;
	printf("Enter the USN\n");
	scanf("%s", usn);
	hash_index = hash(usn);
	if (!strcmp(s[hash_index].USN, usn))	
	{
		printf("USN: %s\nName: %s\nMarks: %f\n", s[hash_index].USN, s[hash_index].name, s[hash_index].marks);
		printf("Attendance: %d\n", s[hash_index].att);
	}
	else
	{
		int i;
		index = hash_index;
		for (i = 1; s[hash_index].USN[0] != '\0'; i++)
		{
			h_idx = (int)(index + pow(-1, i - 1) * pow((i + 1) / 2, 2)) % MAX;
			//For linear probing:
			//hash_index = (index + i) % MAX;
			if (h_idx < 0)
				h_idx += MAX;
			hash_index = h_idx;
			if (!strcmp(s[hash_index].USN, usn))
			{
				printf("USN: %s\nName: %s\nMarks: %f\n", s[hash_index].USN, s[hash_index].name, s[hash_index].marks);
				printf("Attendance: %d\n", s[hash_index].att);
				return;
			}
		}
		printf("Entry not found\n");
	}
}
void display()
{
	int i;
	for (i = 0; i < MAX; i++)
	{
		if (s[i].USN[0] != 0)
		{
			printf("USN: %s\n", s[i].USN);
			printf("Name: %s\n", s[i].name);
			printf("Marks: %f\nAttendance: %d\n", s[i].marks, s[i].att);
		}
	}
}
size_t hash(char usn[])
{
	size_t hash_index = 0, ten = 1;
	int i;
	//Ignoring first three characters (1MS) - common in all USN
	for (i = 3; i < strlen(usn); i++)
	{
		if (isalpha(usn[i]))
		{
			tolower(usn[i]);
			hash_index += ten * (usn[i] - 'a');
		}
		else
			hash_index += ten * usn[i];
		ten *= 10;
	}
	hash_index = hash_index % MAX;
	return (hash_index >= 0) ? hash_index: hash_index + MAX;
}
