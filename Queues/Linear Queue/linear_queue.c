#include"lqueue.h"
void main()
{
	int choice;
	char item;
	Q q;
	q.front =0;
	q.rear = -1;
	while(1)
	{
		printf("Enter\n1.Enqueue\n2.Dequeue\n3.Display\n4.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf(" %c", &item);
				enqueue(&q, item);
				break;
			case 2:
				printf("Deleted item : %c\n", dequeue(&q));
				break;
			case 3:
				display(&q);
				break;
			default:
				exit(0);
		}
	}
}
