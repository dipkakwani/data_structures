#ifndef QUEUELL_H
#define QUEUELL_H
#include<stdio.h>
#include<stdlib.h>
struct Node
{
	int info;
	struct Node* next;	
};
typedef struct Node* NODE;
int isEmpty(NODE front);
NODE get_node();
void free_node(NODE temp);
NODE insert_rear(NODE front, int item);
NODE delete_front(NODE front);
void display(NODE front);
#endif
