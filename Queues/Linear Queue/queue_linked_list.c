#include"queuell.h"
void main()
{
	NODE front = NULL;
	int choice, item;
	while(1)
	{
		printf("Enter\n1.Enqueue\n2.Dequeue\n3.Display\n4.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter item\n");
				scanf("%d", &item);
				front = insert_rear(front, item);
				break;
			case 2:
				front = delete_front(front);
				break;
			case 3:
				display(front);
				break;
			default:
				exit(0);
		}
	}
}
