#include"lqueue.h"
int isEmpty(Q *q)
{
	return (q->rear < q->front);
}
int isFull(Q *q)
{
	return (q->rear == SIZE - 1);
}
void enqueue(Q *q, int item)
{
	if(isFull(q))
	{
		printf("Queue Overflow\n");
		return;
	}
	q->arr[++(q->rear)] = item;
}
void display(Q *q)
{
	int i;
	if(isEmpty(q))
	{
		printf("Empty Queue\n");
		return;
	}
	for(i = q->front; i <= q->rear; i++)
		printf("%d\n", q->arr[i]);
}
int dequeue(Q *q)
{
	int del;
	if(isEmpty(q))
	{
		printf("Queue Underflow\n");
		return 0;
	}
	del = q->arr[q->front];
	(q->front)++;
	if(q->front > q-> rear)			//Dequeued the only element present in the queue
	{
		q->front = 0;
		q->rear = -1;
	}
	return del;
	
}
