#include<stdio.h>
#include<math.h>
#include<string.h>

//Evaluate expression with 2 operands and an operator
double op(double op1, double op2, char sym)
{
	switch(sym)
	{
		case'+':	return (op1 + op2);
		case'-':	return (op1 - op2);
		case'*':	return (op1 * op2);
		case'/':	return (op1 / op2);
		case'$':
		case'^':	return (pow(op2, op1));
	}
}

//Push to Stack
void push(double s[], int *top, double item)
{
	s[++(*top)] = item;
}

//Pop from Stack
double pop(double s[], int *top)
{
	double del;
	del = s[*top];
	--(*top);
	return del;
}

//Checks if sym is a digit
int isDigit(char sym)
{
	return (sym >='0' && sym <= '9');
}
void main()
{
	char sym, postfix[50];
	double op1, op2, res, s[50];
	int i, top = -1;
	printf("Enter Postfix expression\n");
	scanf("%s", postfix);
	for(i = 0; i < strlen(postfix); i++)
	{
		sym = postfix[i];
		if(isDigit(sym))
			push(s, &top, sym - '0');
		else
		{
			op2 = pop(s, &top);
			op1 = pop(s, &top);
			res = op(op1, op2, sym);
			push(s, &top, res);
		}
	}
	res = pop(s, &top);
	printf("%lf", res);
}
