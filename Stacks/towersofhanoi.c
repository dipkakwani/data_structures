#include<stdio.h>
int count = 0;
void tower(int n, char source, char temp, char dest)
{
	if(n == 1)
	{
		printf("Move disk %d from %c to %c\n", n, source, dest);
		count ++;
		return;
	}
	tower(n - 1, source, dest, temp);
	printf("Move disk %d from %c to %c\n", n, source, dest);
	count ++;
	tower(n - 1, temp, source, dest);
}
void main()
{
	int n;
	printf("Enter number of disks\n");
	scanf("%d", &n);
	tower(n, 'A', 'B', 'C');
	printf("\nTotal number of steps: %d\n", count);
}
