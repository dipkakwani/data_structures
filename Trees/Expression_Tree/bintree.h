#ifndef BIN_H
#define BIN_H
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
struct Node
{
	char info;
	struct Node* left;
	struct Node* right; 
};
typedef struct Node* NODE;
NODE get_node();
void preorder(NODE);
void inorder(NODE);
void postorder(NODE);
NODE insert(NODE, char);
#endif
