#include"cllist.h"
void main()
{
	NODE rear = NULL;
	int choice, item, pos;
	while(1)
	{
		printf("Enter\n1.Insert front\n2.Insert rear\n3.Delete front\n4.Delete rear\n5.Display\n6.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter item to be inserted\n");
				scanf("%d", &item);
				rear = insert_front(rear, item);
				break;
			case 2: 
				printf("Enter item to be inserted\n");
				scanf("%d", &item);
				rear = insert_rear(rear, item);
				break;
			case 3:
				rear = delete_front(rear);
				break;
			case 4:
				rear = delete_rear(rear);
				break;
			case 5:
				display(rear);
				break;
			default:
				exit(0);
		}
	}
}
