#include"doublyll.h"
void main()
{
	NODE head;
	int choice, item, pos;
	head = get_node();
	head->info = 0;
	head->next = NULL;
	head->prev = NULL;
	while(1)
	{
		printf("Enter your choice\n 1.Insert Front\n 2.Insert Rear\n 3.Delete Front\n 4.Delete Rear\n 5.Display\n 6.Insert at Position\n 7.Exit\n");
		scanf("%d", &choice);
		switch(choice)
		{
			case 1:
				printf("Enter the item\n");
				scanf("%d", &item);
				head = insert_front(head, item);
				break;
		
			case 2:
				printf("Enter the item\n");
				scanf("%d", &item);
				head = insert_rear(head, item);
				break;
			case 3:
				head = delete_front(head);
				break;
			case 4:
				head = delete_rear(head);
				break;
			case 5:
				display(head);
				break;
			case 6:
				printf("Enter the item\n");
				scanf("%d", &item);
				printf("Enter position\n");
				scanf("%d", &pos);
				head = insert_pos(head, item, pos);
				break;
			default:
				exit(0);
		}
	}
}
