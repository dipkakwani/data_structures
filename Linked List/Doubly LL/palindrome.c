#include"doublyll.h"
#include<string.h>
int palindrome(char s[])
{
	NODE head, cur1, cur2;
	int i;
	head = get_node();
	head->info = 0;
	head->prev = head->next = NULL;
	for (i = 0; i < strlen(s); i++)
		head = insert_rear(head, s[i]);
	//Make it circular
	for (cur1 = head; cur1->next != NULL; cur1 = cur1->next);
	cur1->next = head;
	head->prev = cur1;
	for (cur1 = head->next, cur2 = head->prev, i = 0; i < head->info / 2; i++)
	{
		if (cur1->info != cur2->info)
			return 0;
		cur1 = cur1->next;
		cur2 = cur2->prev;
	}
	return 1;
}
void main()
{
	char s[30];
	printf("Enter the string\n");
	scanf("%s", s);
	if (palindrome(s))
		printf("Palindrome\n");
	else
		printf("Not a palindrome\n");
}
