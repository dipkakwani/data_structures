#include"doublyll.h"
void free_node(NODE temp)
{
	free(temp);
}
NODE get_node()
{
	NODE temp;
	temp = (NODE) malloc(sizeof(struct node));
	if (temp == NULL)
		printf("Memory can not be allocated\n");
	return temp;
}
int isEmpty(NODE head)
{
	return (head->next == NULL);
}
NODE insert_front(NODE head, int item)
{
	NODE temp;
	temp = get_node();
	temp->info = item;
	temp->next = head->next;
	temp->prev = head;
	if (head->next != NULL)
		head->next->prev = temp;
	head->next = temp;
	(head->info)++;
	return head;
}
NODE insert_rear(NODE head, int item)
{
	NODE temp, cur;
	temp = get_node();
	temp->info = item;
	temp->next = NULL;
	for (cur = head; cur->next != NULL; cur = cur->next);
	cur->next = temp;
	temp->prev = cur;
	(head->info)++;
	return head;
}
NODE insert_pos(NODE head, int item, int pos)
{
	NODE temp, cur, pre;
	int i;
	if (pos == 1)
		return insert_front(head, item);
	if (pos < 1 || pos > (head->info + 1))
	{
		printf("Invalid position\n");
		return head;
	}
	temp = get_node();
	temp->info = item;
	for (i = 1, cur = head->next, pre = head; cur != NULL && i < pos; i++)
	{
		pre = cur;
		cur = cur->next; 
	}
	temp->next = cur;
	temp->prev = pre;
	pre->next = temp;
	if (cur != NULL)
		cur->prev = temp;
	return head;
}
NODE delete_front(NODE head)
{
	NODE temp;
	if (isEmpty(head))
	{
		printf("Empty List\n");
		return head;
	}
	temp = head->next;
	(head->info)--;
	printf("Item deleted: %d\n", temp->info);
	if (temp->next == NULL)
	{
		free_node(temp);
		head->next = NULL;
		return head;
	}
	head->next = temp->next;
	temp->next->prev = head;
	free_node(temp);
	return head;
}
NODE delete_rear(NODE head)
{
	NODE cur, pre;
	if (isEmpty(head))
	{
		printf("Empty List\n");
		return head;
	}
	for (cur = head->next, pre = head; cur->next != NULL; pre = cur, cur = cur->next);
	printf("Item deleted: %d\n", cur->info);
	free_node(cur);
	pre->next = NULL;
	(head->info)--;
	return head;
}
void display(NODE head)
{
	NODE cur;
	if (isEmpty(head))
		printf("Empty List\n");
	for (cur = head->next; cur != NULL; cur = cur->next)
		printf("%d\n", cur->info);
}
