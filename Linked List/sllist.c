#include"sllist.h"
int isEmpty(NODE front)
{
	return (front == NULL);
}
NODE get_node()
{
	NODE temp;
	temp = (NODE)malloc(sizeof(struct node));
	if(temp == NULL)
		printf("Not enough memory!\n");
	return temp;
}
void free_node(NODE temp)
{
	free(temp);
}
NODE insert_front(NODE front, int item)
{
	NODE temp;
	temp = get_node();
	if(temp != NULL)
	{
		temp->info = item;
		temp->next = front;
		return temp;
	}
	return NULL;
}
NODE insert_rear(NODE front, int item)
{
	NODE temp;
	NODE cur;
	temp = get_node();
	if(temp != NULL)
	{
		temp->info = item;
		temp->next = NULL;
		if(front == NULL)
			return temp;
		cur = front;
		while(cur->next!= NULL)
			cur = cur->next;
		cur->next = temp;
		printf("%d\n", front->info);
		return front;
	}
	return NULL;
}
NODE insert_pos(NODE front, int item, int pos)
{
	NODE temp;
	NODE cur;
	NODE pre;
	int i;
	temp = get_node();
	if(temp != NULL)
	{
		cur = front;
		pre = NULL;
		temp->info = item;
		if(pos == 1)
		{
			temp->next = front;
			return temp;
		}
		for(i = 1; cur != NULL && i < pos; ++i)
		{
			pre = cur;
			cur = cur->next;
		}
		if((cur == NULL && i < pos) || pos < 1)
		{
			printf("Invalid Position\n");
			free_node(temp);
			return front;
		}
		temp->next = cur;
		pre->next = temp;
		return front;
	}
	return NULL;
}
NODE delete_front(NODE front)
{
	NODE temp;
	if(isEmpty(front))
	{
		printf("No element to delete\n");
		return front;
	}	
	printf("The item deleted %d\n", front->info);
	temp = front;
	front = front->next;
	free_node(temp);
	return front;
	
}
NODE delete_rear(NODE front)
{
	NODE cur, pre;
	if(isEmpty(front))
	{
		printf("No element to delete\n");
		return front;
	}
	cur = front;
	pre = NULL;
	if(cur->next == NULL)
	{
		free_node(front);
		front = NULL;
		return front;
	}
	while(cur->next != NULL)
	{
		pre = cur;
		cur = cur->next;
	}
	printf("The item deleted %d\n", cur->info);
	pre->next = NULL;
	free_node(cur);
	return front;
}
NODE delete_pos(NODE front, int pos)
{
	NODE cur;
	NODE pre;
	int i;
	cur = front;
	pre = NULL;
	if(isEmpty(front))
	{
		printf("No element to delete\n");
		return front;
	}
	if(pos == 1)
	{
		printf("The element deleted %d\n", cur->info);
		front = front->next;
		return front;
	}
	for(i = 1; cur != NULL && i < pos; ++i)
	{
		pre = cur;
		cur = cur->next;
	}
	if((cur == NULL && i <= pos) || pos < 1)
	{
		printf("Invalid Position\n");
		return front;
	}
	printf("The element deleted %d\n", cur->info);
	pre->next = cur->next;
	free_node(cur);
	return front;	
}
void display(NODE front)
{
	NODE cur;
	if(isEmpty(front))
	{
		printf("Empty List\n");
		return;
	}
	cur = front;
	while(cur != NULL)
	{
		printf("%d\n", cur->info);
		cur = cur->next;
	}
}
void search(NODE front, int item)
{
	NODE cur;
	int pos = 1;
	cur = front;
	while(cur != NULL && cur->info != item)
	{
		cur = cur->next;
		pos++;
	}
	if(cur == NULL)
		printf("Element not found\n");
	else
		printf("Element found at %d\n", pos);
}
