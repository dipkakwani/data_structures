#include"linked_list.h"
NODE rev(NODE front)
{
	NODE cur, prev, temp;
	for (cur = front, prev = NULL; cur != NULL;)
	{
		if (cur->next == NULL)
			front = cur;
		temp = cur->next;
		cur->next = prev;
		prev = cur;
		cur = temp;
	}
	return front;
}
void main()
{
	NODE front = NULL;
	int item, n;
	printf("Enter size of the linked list\n");
	scanf("%d", &n);
	while (n--)
	{
		printf("Enter item\n");
		scanf("%d", &item);
		front = insert_rear(front, item);
	}
	front = rev(front);
	printf("Reversed List\n");
	display(front);
}
